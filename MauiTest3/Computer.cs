namespace MauiTest3; 

public class Computer : Player {
    private IList<Cell> _availableCells;
    private readonly ComputerDifficultyEnum _difficulty;

    public Computer() { }

    public Computer(string name, Color color, ComputerDifficultyEnum difficulty) : base(name, color) {
        _difficulty = difficulty;
    }

    public void PlayTurn() {
        var turn = new Thread(() => {
          Thread.Sleep(1000);
                switch (_difficulty) {
                    case ComputerDifficultyEnum.Easy:
                        easyCompute();
                        break;
                }
            }
        );

        turn.Start();
    }

    private void easyCompute() {
        var availableBarFound = false;

        while (!availableBarFound) {
            var randomCell = new Random();
            var cellIndex = randomCell.Next(_availableCells.Count());
            var cellBars = _availableCells.ElementAt(cellIndex).GetCellBars();

            var randomBar = new Random();
            var barIndex = randomBar.Next(cellBars.Count());
            var selectedBar = cellBars.ElementAt(barIndex);

            if (selectedBar.GetOwner() == null) {
                availableBarFound = true;
                SelectCellBar(selectedBar);
            }
        }
    }

    private void SelectCellBar(CellBar cellBar) {
        MainThread.BeginInvokeOnMainThread(() => { cellBar.StartHovering(this); });

        Thread.Sleep(1000);

        MainThread.BeginInvokeOnMainThread(() => { cellBar.BarHasBeenTapped(this, null); });
    }

    public void SetAvailableCells(IList<Cell> cells) {
        _availableCells = cells;
    }
}