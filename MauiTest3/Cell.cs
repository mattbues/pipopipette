﻿namespace MauiTest3; 

public delegate void NotifyBarIsSelectedByPlayerDelegate(Player owner, CellBar cellBar);

public class Cell {
    private readonly IList<CellBar> _cellBars;

    private readonly BoxView _cellBox;

    public Cell(BoxView cellBox) {
        _cellBox = cellBox;
        _cellBars = new List<CellBar>();
    }

    public event NotifyCellStatusChangedDelegate NotifyCellStatusChangedEvent;

    public void AddBar(CellBar cellBar) {
        _cellBars.Add(cellBar);
        cellBar.updateBorderStatus();
        cellBar.NotifyBarIsSelectedByPlayerEvent += PlayerSelectedBar;
    }

    private void PlayerSelectedBar(Player owner, CellBar cellBar) {
        var cellIsFilled = true;

        for (var i = 0; i < _cellBars.Count(); i++) {
            var barOwner = _cellBars.ElementAt(i).GetOwner();

            if (barOwner == null) cellIsFilled = false;
        }

        if (cellIsFilled) {
            _cellBox.Color = owner.GetColor();
            _cellBox.Opacity = 0.5;

            owner.UpdateScore();
        }

        NotifyCellStatusChangedEvent?.Invoke(this, cellBar, cellIsFilled);
    }

    public IList<CellBar> GetCellBars() {
        return _cellBars;
    }
}