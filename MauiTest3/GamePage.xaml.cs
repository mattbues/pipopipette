﻿namespace MauiTest3;

public delegate void NotifyScoreDelegate();

public delegate void NotifyEndOfMatchDelegate();

public delegate void NotifySwitchTurnDelegate(Player currentPlayer);

public partial class GamePage {
    private Game _game;

    public GamePage() {
        InitializeComponent();

        gameLayout.IsVisible = false;
        resetBtn.IsEnabled = false;
        p2ComputerDifficulty.IsVisible = false;

        FillColorPicker(p1Color, 0);
        FillColorPicker(p2Color, 1);

        FillComputerDifficultyPicker(0);
    }

    private void FillComputerDifficultyPicker(int defaultIndex) {
        // Créez une liste de couleurs à partir de l'énumération Colors
        var difficultiesList = new List<ComputerDifficultyEnum> {
            ComputerDifficultyEnum.Easy
        };

        // Liez le Picker à la liste de couleurs
        p2ComputerDifficulty.ItemsSource = difficultiesList;
        p2ComputerDifficulty.SelectedIndex = defaultIndex;
    }

    private void FillColorPicker(Picker colorPicker, int defaultIndex) {
        // Créez une liste de couleurs à partir de l'énumération Colors
        var colorList = new List<ColorItem> {
            new("Red", Colors.Red),
            new("Blue", Colors.Blue),
            new("Green", Colors.Green)
        };

        // Liez le Picker à la liste de couleurs
        colorPicker.ItemsSource = colorList;
        colorPicker.SelectedIndex = defaultIndex;
    }

    private void StartGame(object sender, EventArgs e) {
        _game = new Game(PipetteGrid);

        _game.AddPlayer(p1Name.Text, ((ColorItem)p1Color.SelectedItem).ColorValue);

        if (p2Switch.IsChecked)
            _game.AddComputer("Computer", Colors.Gold, (ComputerDifficultyEnum)p2ComputerDifficulty.SelectedItem);
        else
            _game.AddPlayer(p2Name.Text, ((ColorItem)p2Color.SelectedItem).ColorValue);

        _game.setGameSize((int)gameSizeSlider.Value);

        _game.NotifyEndOfMatchEvent += OnMatchEnded;
        _game.NotifySwitchTurnEvent += OnSwitchingTurn;

        foreach (var player in _game.GetPlayers()) player.NotifyScoreEvent += ScoreUpdatedEvent;

        gameEntries.IsVisible = false;
        gameLayout.IsVisible = true;
        startGameBtn.IsEnabled = false;
        resetBtn.IsEnabled = true;

        scorePlayer1.Text = _game.GetPlayer(0).GetScore().ToString();
        scorePlayer2.Text = _game.GetPlayer(1).GetScore().ToString();

        scorePlayer1.TextColor = _game.GetPlayer(0).GetColor();
        scorePlayer2.TextColor = _game.GetPlayer(1).GetColor();

        namePlayer1.Text = _game.GetPlayer(0).GetName();
        namePlayer2.Text = _game.GetPlayer(1).GetName();

        namePlayer1.TextColor = _game.GetPlayer(0).GetColor();
        namePlayer2.TextColor = _game.GetPlayer(1).GetColor();

        _game.StartGame();
    }

    private void Reset(object sender, EventArgs e) {
        ResetMatch();
    }

    private void ResetMatch() {
        gameEntries.IsVisible = true;
        gameLayout.IsVisible = false;
        startGameBtn.IsEnabled = true;
        resetBtn.IsEnabled = false;

        PipetteGrid.Children.Clear();

        scorePlayer1.Text = "0";
        scorePlayer2.Text = "0";
    }

    private void OnMatchEnded() {
        ResetMatch();
    }

    private void OnSwitchingTurn(Player currentPlayer) {
        currentPlayerLabel.TextColor = currentPlayer.GetColor();
        currentPlayerLabel.Text = currentPlayer.GetName() + " is playing";
    }

    private void ScoreUpdatedEvent() {
        scorePlayer1.Text = _game?.GetPlayer(0).GetScore().ToString() ?? string.Empty;
        scorePlayer2.Text = _game?.GetPlayer(1).GetScore().ToString() ?? string.Empty;
    }

    private void OnGameSizeSliderValueChanged(object sender, ValueChangedEventArgs args) {
        var value = (int)args.NewValue;
        gameSizeLabel.Text = string.Format("{0}", value);
    }

    private void OnPlayer2SwitchChanged(object sender, EventArgs e) {
        p2ComputerDifficulty.IsVisible = p2Switch.IsChecked;
        p2Human.IsVisible = !p2Switch.IsChecked;
    }
}