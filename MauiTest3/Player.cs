﻿namespace MauiTest3; 

public class Player {
    private Color _color;

    private string _name;
    private int _score;

    public Player(string name, Color color) {
        _name = name;
        _color = color;
        _score = 0;
    }

    public Player() { }
    public event NotifyScoreDelegate NotifyScoreEvent;

    public void SetName(string name) {
        _name = name;
    }

    public string GetName() {
        return _name;
    }

    public void SetColor(Color color) {
        _color = color;
    }

    public Color GetColor() {
        return _color;
    }

    public void SetScore(int score) {
        _score = score;
    }

    public int GetScore() {
        return _score;
    }

    public void UpdateScore() {
        _score++;
        NotifyScoreEvent?.Invoke();
    }
}