﻿namespace MauiTest3; 

public class ColorItem {
    public ColorItem(string name, Color color) {
        Name = name;
        ColorValue = color;
    }

    public string Name { get; set; }
    public Color ColorValue { get; set; }
}