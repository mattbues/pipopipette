﻿namespace MauiTest3;

public partial class MainPage {
    public MainPage() {
        InitializeComponent();
    }

    private void Play(object sender, EventArgs e) {
        var gamePage = new GamePage();
        contentPresenter.Content = gamePage.Content;
    }
}