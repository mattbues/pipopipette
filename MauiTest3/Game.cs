﻿namespace MauiTest3; 

public delegate void NotifyBarIsSelectedDelegate(CellBar cellBar, CellBarEventEnum barEvent, object sender);

public delegate void NotifyCellStatusChangedDelegate(Cell cell, CellBar cellBar, bool isFilled);

public class Game {
    private readonly IList<Cell> _availableCells;
    private readonly IList<IList<Cell>> _cells;
    private Player _currentPlayer;
    private readonly IList<Cell> _filledCells;
    private readonly Grid _gameGrid;
    private readonly IList<Player> _players;
    private (int, int) _turn;
    private (bool, bool) _turnFilledCells; // paire de boules
    private int barsSize;

    private int boxesSize;
    private int gameSize;

    public Game(Grid grid) {
        boxesSize = 60;
        barsSize = 20;
        gameSize = 5;

        _players = new List<Player>();
        _gameGrid = grid;

        _cells = new List<IList<Cell>>();
        _availableCells = new List<Cell>();
        _filledCells = new List<Cell>();

        _turn = (0, 0);
        _turnFilledCells = (false, false);
    }

    public event NotifyEndOfMatchDelegate NotifyEndOfMatchEvent;
    public event NotifySwitchTurnDelegate NotifySwitchTurnEvent;

    public void StartGame() {
        CreateGridWithBoxes(gameSize);

        if (_players.ElementAt(1).GetType() == typeof(Computer)) {
            var computer = (Computer)_players.ElementAt(1);
            computer.SetAvailableCells(_availableCells);
        }

        var random = new Random();
        _currentPlayer = _players.ElementAt((int)Math.Floor(random.NextDouble() * 2));

        NotifySwitchTurnEvent?.Invoke(_currentPlayer);

        if (_currentPlayer.GetType() == typeof(Computer)) {
            var computer = (Computer)_currentPlayer;
            computer.PlayTurn();
        }
    }

    public void AddPlayer(string name, Color color) {
        _players.Add(new Player(name, color));
    }

    public void AddComputer(string name, Color color, ComputerDifficultyEnum difficulty) {
        _players.Add(new Computer(name, color, difficulty));
    }

    private void OnBarSelected(CellBar cellBar, CellBarEventEnum barEvent, object sender) {
        if (_currentPlayer.GetType() != sender.GetType() && _currentPlayer.GetType() != typeof(Player)) return;
        switch (barEvent) {
            case CellBarEventEnum.Click:
                cellBar.PlayerSelected(_currentPlayer, ref _turn);
                break;
            case CellBarEventEnum.Hover:
                cellBar.StartHovering(_currentPlayer);
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(barEvent), barEvent, null);
        }
    }

    private async void OnCellStatusChanged(Cell cell, CellBar cellBar, bool isFilled) {
        var highestScorePlayer = new Player();
        var highestScore = 0;

        foreach (var player in _players)
            if (player.GetScore() > highestScore) {
                highestScore = player.GetScore();
                highestScorePlayer = player;
            }

        if (isFilled) _filledCells.Add(cell);

        if (isFilled && highestScorePlayer.GetScore() > _cells.Count() * _cells.Count() / 2) {
            await Application.Current.MainPage.DisplayAlert("The end",
                highestScorePlayer.GetName() + " wins !", "OK");
            NotifyEndOfMatchEvent.Invoke();
        }
        else if (isFilled && _filledCells.Count() == _cells.Count() * _cells.Count()) {
            await Application.Current.MainPage.DisplayAlert("The end", "Draw. Maybe we'll have a winner next time !",
                "OK");
            NotifyEndOfMatchEvent.Invoke();
        }
        else {
            if (_turn.Item1 < _turn.Item2) {
                _turnFilledCells.Item1 = isFilled;
                _turn.Item1++;

                if (cellBar.isBorder()) SwitchPlayerTurn();
            }
            else {
                _turnFilledCells.Item2 = isFilled;
                SwitchPlayerTurn();
            }
        }
    }

    private void SwitchPlayerTurn() {
        if (!(_turnFilledCells.Item1 || _turnFilledCells.Item2)) {
            var newCurrentPlayer = 1 - _players.IndexOf(_currentPlayer);
            _currentPlayer = _players.ElementAt(newCurrentPlayer);
            NotifySwitchTurnEvent.Invoke(_currentPlayer);
        }

        _turnFilledCells.Item1 = false;
        _turnFilledCells.Item2 = false;

        if (_currentPlayer.GetType() == typeof(Computer)) {
            var computer = (Computer)_currentPlayer;
            computer.PlayTurn();
        }
    }

    private void CreateGridWithBoxes(int gameSize) {
        IList<IList<CellBar>> horizontalCellBars = new List<IList<CellBar>>();

        var nbBoxes = gameSize;
        var sizeBoxes = 50;
        var sizeBars = 10;

        for (var row = 0; row < nbBoxes; row++) {
            IList<Cell> cellsRow = new List<Cell>();
            IList<CellBar> verticalBarsRow = new List<CellBar>();
            IList<CellBar> horizontalBarsRow = new List<CellBar>();

            int k, l;
            if (row == 0) {
                _gameGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(sizeBars) });

                for (var col = 0; col < nbBoxes; col++) {
                    int i, j;

                    if (col == 0) {
                        _gameGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(sizeBars) });
                        Draw(Colors.Black, row, col);

                        i = col + 1;
                        j = col + 2;
                    }
                    else {
                        i = 2 * col + 1;
                        j = 2 * col + 2;
                    }

                    _gameGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(sizeBoxes) });
                    var cellBar1 = new CellBar(Draw(Colors.White, row, i));
                    cellBar1.NotifyBarIsSelectedEvent += OnBarSelected;
                    horizontalBarsRow.Add(cellBar1);

                    _gameGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(sizeBars) });
                    Draw(Colors.Black, row, j);
                }

                horizontalCellBars.Add(horizontalBarsRow);

                k = row + 1;
                l = row + 2;
            }
            else {
                k = 2 * row + 1;
                l = 2 * row + 2;
            }

            horizontalBarsRow = new List<CellBar>();

            _gameGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(sizeBoxes) });

            for (var col = 0; col < nbBoxes; col++) {
                int i, j;

                if (col == 0) {
                    _gameGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(sizeBars) });
                    var cellBar2 = new CellBar(Draw(Colors.White, k, col));
                    cellBar2.NotifyBarIsSelectedEvent += OnBarSelected;
                    verticalBarsRow.Add(cellBar2);

                    i = col + 1;
                    j = col + 2;
                }
                else {
                    i = 2 * col + 1;
                    j = 2 * col + 2;
                }

                _gameGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(sizeBoxes) });
                var gameBoxCell = new Cell(Draw(Colors.White, k, i));
                gameBoxCell.NotifyCellStatusChangedEvent +=
                    OnCellStatusChanged;
                cellsRow.Add(gameBoxCell);
                _availableCells.Add(gameBoxCell);

                _gameGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(sizeBars) });
                var cellBar3 = new CellBar(Draw(Colors.White, k, j));
                cellBar3.NotifyBarIsSelectedEvent += OnBarSelected;
                verticalBarsRow.Add(cellBar3);

                cellsRow.ElementAt(col).AddBar(verticalBarsRow.ElementAt(col));
                cellsRow.ElementAt(col).AddBar(verticalBarsRow.ElementAt(col + 1));
            }

            _gameGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(sizeBars) });

            for (var col = 0; col < nbBoxes; col++) {
                int i, j;

                if (col == 0) {
                    _gameGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(sizeBars) });
                    Draw(Colors.Black, l, col);

                    i = col + 1;
                    j = col + 2;
                }
                else {
                    i = 2 * col + 1;
                    j = 2 * col + 2;
                }

                _gameGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(sizeBoxes) });
                var cellBar4 = new CellBar(Draw(Colors.White, l, i));
                cellBar4.NotifyBarIsSelectedEvent += OnBarSelected;
                horizontalBarsRow.Add(cellBar4);

                cellsRow.ElementAt(col).AddBar(horizontalCellBars.ElementAt(row).ElementAt(col));
                cellsRow.ElementAt(col).AddBar(horizontalBarsRow.ElementAt(col));

                _gameGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(sizeBars) });
                Draw(Colors.Black, l, j);
            }

            horizontalCellBars.Add(horizontalBarsRow);

            _cells.Add(cellsRow);
        }
    }

    private BoxView Draw(Color color, int row, int col) {
        var cellView = new BoxView {
            Color = color
        };

        Grid.SetRow(cellView, row);
        Grid.SetColumn(cellView, col);
        _gameGrid.Children.Add(cellView);

        return cellView;
    }

    public Player GetPlayer(int index) {
        return _players.ElementAt(index);
    }

    public IEnumerable<Player> GetPlayers() {
        return _players;
    }

    public void setGameSize(int gameSize) {
        this.gameSize = gameSize;
    }
}