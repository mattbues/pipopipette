﻿namespace MauiTest3; 

public class CellBar {
    private readonly BoxView _bar;
    private bool _border;
    private bool _hovered;
    private Player _owner;

    public CellBar(BoxView bar) {
        _bar = bar;

        _border = false;

        _hovered = false;

        var tapGesture = new TapGestureRecognizer();
        tapGesture.Tapped += BarHasBeenTapped;
        _bar.GestureRecognizers.Add(tapGesture);

        var hoverGesture = new PointerGestureRecognizer();
        hoverGesture.PointerEntered += BarHasBeenHovered;
        _bar.GestureRecognizers.Add(hoverGesture);

        var endedHoverGesture = new PointerGestureRecognizer();
        endedHoverGesture.PointerExited += (_, _) => BarHasEndedHover();
        _bar.GestureRecognizers.Add(endedHoverGesture);
    }

    public event NotifyBarIsSelectedDelegate NotifyBarIsSelectedEvent;
    public event NotifyBarIsSelectedByPlayerDelegate NotifyBarIsSelectedByPlayerEvent;

    private void BarHasEndedHover() {
        _hovered = false;
    }

    private void BarHasBeenHovered(object sender, EventArgs eventArgs) {
        NotifyBarIsSelectedEvent?.Invoke(this, CellBarEventEnum.Hover, sender);
    }

    public void BarHasBeenTapped(object sender, EventArgs eventArgs) {
        NotifyBarIsSelectedEvent?.Invoke(this, CellBarEventEnum.Click, sender);
    }

    public void PlayerSelected(Player player, ref (int, int) turn) {
        if (_owner != null) return;
        turn.Item2++;
        _owner = player;
        _bar.Color = player.GetColor();
        _bar.Opacity = 1;
        NotifyBarIsSelectedByPlayerEvent?.Invoke(_owner, this);
    }

    public void StartHovering(Player player) {
        if (_owner != null) return;
        _hovered = true;

        _bar.Color = player.GetColor();
        _bar.Opacity = 0.3;

        var hoverThread = new Thread(hover);
        hoverThread.Start();
    }

    private void hover() {
        var step = 0.01;
        double direction = 1;

        while (_hovered && _owner == null) {
            MainThread.BeginInvokeOnMainThread(() => { _bar.Opacity += step * direction; });

            if (_bar.Opacity <= 0.3)
                direction = 1;
            else if (_bar.Opacity >= 1) direction = -1;

            Thread.Sleep(5);
        }

        if (_owner == null)
            MainThread.BeginInvokeOnMainThread(() => {
                _bar.Opacity = 1;
                _bar.Color = Colors.White;
            });
    }

    public Player GetOwner() {
        return _owner;
    }

    public void updateBorderStatus() {
        _border = !_border;
    }

    public bool isBorder() {
        return _border;
    }
}